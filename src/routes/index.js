import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Splash from '../pages/splash';
import Login from '../pages/Login';
import WelcomeAuth from '../pages/welcome';
import Whisker from '../pages/whisker';
import Akame from '../pages/akame';
import Anohana from '../pages/anohana';
import Aot from '../pages/aot';
import Another from '../pages/another';
import Assassion from '../pages/assassion';
import Black from '../pages/bc';
import Clannad from '../pages/clannad';
import Demon from '../pages/demon';
import Erased from '../pages/erased';
import Grave from '../pages/grave';
import Haikyuu from '../pages/haikyuu';
import Jujutsu from '../pages/jujutsu';
import Naruto from '../pages/naruto';
import Opm from '../pages/opm';
import Ghoul from '../pages/ghoul';
import Tokyo from '../pages/tokyo';
import Violet from '../pages/violet';
import Watashi from '../pages/watashi';
import Your from '../pages/your';

const Stack = createStackNavigator();

const Route = ()=> {
    return (
        <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
        />
        <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
        />
        <Stack.Screen
            name="Welcome"
            component={WelcomeAuth}
            options={{headerShown : false}}
        />
        <Stack.Screen
            name="A WHISKER AWAY"
            component={Whisker}
        />
        <Stack.Screen
            name="AKAME GA KILL"
            component={Akame}
        />
        <Stack.Screen
            name="ANOHANA"
            component={Anohana}
        />
        <Stack.Screen
            name="ATTACK ON TITAN"
            component={Aot}
        />
        <Stack.Screen
            name="ANOTHER"
            component={Another}
        />
        <Stack.Screen
            name="ASSASSINATION CLASSROOM"
            component={Assassion}
        />
        <Stack.Screen
            name="BLACK CLOVER"
            component={Black}
        />
        <Stack.Screen
            name="CLANNAD"
            component={Clannad}
        />
        <Stack.Screen
            name="DEMON SLAYER"
            component={Demon}
        />
        <Stack.Screen
            name="ERASED"
            component={Erased}
        />
        <Stack.Screen
            name="GRAVE OF THE FIREFLIES"
            component={Grave}
        />
        <Stack.Screen
            name="HAIKYUU!!"
            component={Haikyuu}
        />
        <Stack.Screen
            name="JUJUTSU KAISEN!!"
            component={Jujutsu}
        />
        <Stack.Screen
            name="NARUTO"
            component={Naruto}
        />
        <Stack.Screen
            name="ONE PUNCH MAN"
            component={Opm}
        />
        <Stack.Screen
            name="TOKYO GHOUL"
            component={Ghoul}
        />
        <Stack.Screen
            name="TOKYO REVENGERS"
            component={Tokyo}
        />
        <Stack.Screen
            name="VIOLET EVERGARDEN"
            component={Violet}
        />
        <Stack.Screen
            name="WATASHI NI TENSHI GA MAIORITA!"
            component={Watashi}
        />
        <Stack.Screen
            name="YOUR NAME"
            component={Your}
        />
        </Stack.Navigator>
    );
};

export default Route;