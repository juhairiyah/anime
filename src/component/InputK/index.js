import React from 'react';
import {Text, TextInput, StyleSheet} from 'react-native';

const InputK = ({label, placeholder, onChangeText, value, nameState, keyType}) => {
    return(
        <>
        <Text style={styles.label}>{label}</Text>
        <TextInput placeholder={placeholder}
        secureTextEntry={true} style={styles.default} value="abc"
        style={styles.Inputarea}
        keyboardType={keyType}
        onChangeText={text => onChangeText(nameState, text)}
        value={value}
        />
        </>
    )
}
export default InputK;

const styles = StyleSheet.create({
    label : {
        fontSize : 15,
        marginBottom : 5,
    },
    Inputarea : {
        textAlignVertical : 'top',
        borderColor : 'black',
        padding : 10,
        marginBottom : 10,
        backgroundColor :'black',
        width : 300,
        borderRadius : 25,
        paddingHorizontal : 16,
        fontSize : 16,
        color:'white'
    }
});
