import firebase from 'firebase';


var firebaseConfig = {
    apiKey: "AIzaSyCN-7aYvJvLo4zunB3oxd4BLlPqZdCICTE",
    authDomain: "anime-497e9.firebaseapp.com",
    databaseURL: "https://anime-497e9-default-rtdb.firebaseio.com",
    projectId: "anime-497e9",
    storageBucket: "anime-497e9.appspot.com",
    messagingSenderId: "223321185613",
    appId: "1:223321185613:web:de92db2483035df2a1eb04",
    measurementId: "G-YWQ442H4ME"
  };

  firebase.initializeApp(firebaseConfig);

  const FIREBASE = firebase;

  export default FIREBASE;