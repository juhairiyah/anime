import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class watashi extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Miyako Hoshino ialah seorang mahasiswi yang sangat pemalu terhadap orang-orang yang tidak dikenalinya. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Ketika adik perempuannya, Hinata, membawa teman sekelasnya yang bernama Hana Shirosaki ke rumah, Miyako tersihir oleh kelucuan Hana, dan mulai mendandaninya dengan pakaian cosplay sebagai ganti camilan. </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Seri ini kemudian menceritakan kedekatan Miyako dengan Hana dan teman sekelas Hinata yang lain.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:4},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:5},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/watashini.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default watashi;