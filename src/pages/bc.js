import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class bc extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Asta dan Yuno adalah para anak yatim piatu yang dibesarkan bersama sejak lahir setelah ditinggalkan di panti asuhan gereja di desa pinggiran Kerajaan Semanggi pada saat yang sama. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Di dunia tempat setiap orang dilahirkan dengan kemampuan untuk memanfaatkan Mana dalam bentuk Kekuatan Sihir (魔力 Maryoku), Asta adalah satu-satunya orang yang tidak memiliki kemampuan sihir dan karenanya dia mengimbangi kekurangan ini melalui pelatihan fisik. </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Sebaliknya, Yuno dilahirkan sebagai anak ajaib dengan kekuatan sihir yang luar biasa dan bakat untuk mengendalikan sihir angin. </Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Termotivasi oleh keinginan untuk menjadi kaisar sihir—sosok penting yang kedudukannya berada di bawah Raja Kerajaan Semanggi—yang berikutnya, kedua pemuda ini memulai persaingan yang sehat. </Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Yuno memperoleh grimoire (buku sihir) empat daun yang sebelumnya dimiliki oleh kaisar sihir pertama dari Kerajaan Semanggi.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Asta, meskipun tidak memiliki kemampuan sihir, mendapatkan grimoire lima daun yang misterius (yang tidak dapat diidentifikasi dengan jelas oleh orang lain karena kondisinya) dan berisi pedang elf misterius dan kemampuan anti-sihir yang misterius. </Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Kemudian, dirinya dan Yuno bergabung dengan Ordo Ksatria Sihir sebagai langkah pertama untuk memenuhi ambisi mereka. </Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Sepanjang cerita, Asta dan Yuno melakukan berbagai petualangan sekaligus membuat nama mereka semakin dikenal di seluruh Kerajaan Semanggi. </Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:9},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:10},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/bc.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default bc;