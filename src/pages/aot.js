import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class whisker extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Anime Attack on Titan bercerita pada berabad-abad yang lalu, umat manusia dibantai hingga hampir punah oleh makhluk menyerupai manusia mengerikan yang disebut Titan, memaksa manusia untuk bersembunyi dalam ketakutan di balik tembok konsentris yang sangat besar.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Apa yang membuat raksasa-raksasa ini benar-benar menakutkan adalah bahwa selera terhadap daging manusia mereka tidak lahir dari rasa lapar, tetapi apa yang tampak seperti kesenangan belaka.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Untuk memastikan kelangsungan hidup mereka, sisa-sisa umat manusia mulai hidup dalam penghalang pertahanan, menghasilkan seratus tahun tanpa satupun pertemuan dengan Titan. </Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Tapi, ketenangan yang rapuh itu segera hancur ketika Titan Kolosal berhasil menembus tembok luar yang seharusnya tak tertembus, menyalakan kembali perjuangan untuk bertahan hidup melawan kekejian dari pemakan manusia.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Setelah menyaksikan kehilangan seseorang berharga dengan mengerikan di tangan makhluk-makhluk yang menyerang, Eren Yeager (diisi suara oleh Yuki Kaji) mendedikasikan hidupnya untuk pemberantasan mereka dengan mendaftar ke Pasukan Pengintai, sebuah unit militer elit yang memerangi para Titan dengan tanpa ampun di luar perlindungan tembok.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Eren didampingi saudari angkatnya, Mikasa Ackerman (diisi suara oleh Yui Ishikawa), dan teman masa kecilnya, Armin Arlert (diisi suara oleh Marina Inoue), bergabung dalam perang brutal melawan para Titan dan berlomba untuk menemukan cara mengalahkan mereka sebelum tembok-tembok terakhir runtuh.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:7},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:8},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/aot.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                       <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default whisker;