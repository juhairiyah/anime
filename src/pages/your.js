import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class your extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Mitsuha Miyamizu, seorang siswi sekolah menengah atas yang tinggal di desa fiktif bernama Itomori di daerah pegunungan Hida Prefektur Gifu, mulai bosan dengan kehidupannya di pedesaan tempat dia lahir dan berharap dapat terlahir menjadi pemuda tampan yang hidup di Tokyo pada kehidupan selanjutnya. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Kemudian, Taki Tachibana, seorang siswa sekolah menengah atas yang tinggal di Tokyo, terbangun dari tidurnya dan menyadari bahwa dirinya adalah Mitsuha, yang entah bagaimana bisa masuk ke dalam tubuh Taki.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Taki dan Mitsuha menyadari bahwa mereka berdua saling memasuki tubuh satu sama lain. Mereka mulai berkomunikasi satu sama lain dengan saling meninggalkan catatan di kertas maupun melalui memo di ponsel mereka. Seiring dengan berjalannya waktu, mereka semakin terbiasa dengan pertukaran tubuh ini serta mulai mencampuri kehidupan satu sama lain.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Mitsuha membantu Taki untuk menjalin hubungan dengan seorang wanita rekan kerjanya yang bernama Miki Okudera, sehingga akhirnya Taki dapat berkencan dengan Miki. </Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Sementara itu, Taki membantu Mitsuha agar lebih dikenal di sekolahnya. Mitsuha kemudian memberitahu Taki mengenai sebuah komet yang diramalkan akan melintas dekat Bumi dalam beberapa hari mendatang, dan betapa tertariknya dirinya untuk dapat melihatnya, karena waktunya bertepatan dengan festival di desa Itomori.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Pada suatu saat, nenek dari Mitsuha bertanya kepada Taki yang berada di dalam tubuh Mitsuha, "Kamu masih bermimpi, kan?", dan bercerita bahwa keturunan Miyamizu terkadang mengalami mimpi tentang kehidupan orang lain. </Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Keesokannya, Taki tiba-tiba kembali terbangun di tubuhnya sendiri. Saat bertemu dengan Miki, kencannya berakhir gagal. Miki meninggalkan Taki, mengatakan bahwa dirinya menyadari bahwa ada orang lain yang dipikirkan oleh Taki. </Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Semenjak itu, Taki dan Mitsuha tidak pernah bertukar tubuh lagi. Taki kemudian mencoba menghubungi Mitsuha, namun tidak berhasil. Dia akhirnya memutuskan untuk menemui Mitsuha secara langsung dengan mengunjungi tempat kelahirannya.</Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Tanpa mengetahui apa nama desa Mitsuha dan di mana lokasinya, Taki melakukan perjalanan ke berbagai daerah di Hida, dengan hanya mengandalkan sketsa pemandangan desa yang dia lukis berdasarkan ingatannya.</Text>,nomor:9},
                {isi:<Text style={styles.teks}>         Akhirnya, di sebuah restoran yang dia kunjungi bersama Miki dan teman sekolahnya Tsukasa Fujii, seorang pelayan mengenali sketsa Taki sebagai desa Itomori, dan mengatakan bahwa desa tersebut dahulunya sangat indah. </Text>,nomor:10},
                {isi:<Text style={styles.teks}>         Namun kini desa tersebut dan wilayah di sekitarnya telah hancur serta menjadi kawah raksasa karena dihantam pecahan komet Tiamat yang jatuh tiga tahun lalu. Saat membaca catatan kejadian, Taki menyadari bahwa kejadian ini telah menewaskan sepertiga penduduk.</Text>,nomor:11},
                {isi:<Text style={styles.teks}>         Taki juga menemukan nama Mitsuha dan juga kedua temannya, Katsuhiko Teshigawara dan Sayaka Natori, yang tertera dalam daftar korban. Taki kemudian diantarkan oleh pelayan tersebut menuju lokasi kawah. Di sana, Taki mencari memo yang pernah Mitsuha buat pada ponselnya untuk memastikan bahwa dirinya tidak sedang bermimpi. Namun ternyata semuanya telah menghilang.</Text>,nomor:12},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:13},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:14},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/yourname.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default your;