import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class assassion extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Awal cerita anime ini memperlihatkan situasi kelas yang sangat menegangkan. Tiba-tiba ada seorang guru berbentuk sangat aneh. Kepala bulat seperti balon, tubuh kecil dan memiliki tentakel seperti gurita.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Tiba-tiba para siswa mengeluarkan sebuah pistol dan menembaki gurunya itu secara membabi buta. Namun, tak ada yang bisa mengenainya karena gurunya itu sangat lincah dan cepat.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Namun peluru yang digunakan bukan yang bisa melukai manusia. Pelurunya diberi nama BB Ammo. Karena hanya jenis peluru itulah yang bisa melukai guru aneh tersebut.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Rupanya kekuatan dari guru itu adalah memiliki kecepatan 20 mach atau setara 24.500Km/jam.Kelas 3E SMP Kunugigaoka mendapatkan misi membunuh gurunya itu dalam jangka waktu satu tahun.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Dalam sinopsis Ansatsu Kyoushitsu ini ada dua kejadian yang membuat mereka dilatih sebagai pembunuh. Pertama karena 70% bulan tiba-tiba hancur berkeping-keping.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Kedua adalah tersangka peledakan tiba-tiba datang ke kelas dan akan mengajar di sana. Selain belajar, kelas 3E juga harus membunuh guru tersebut.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Hal ini juga merupakan perintah dari kementerian pertahanan Jepang. Salah satu perwakilan kementerian adalah Karasuma.</Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Rupanya jika tidak ada yang membunuh guru itu maka bumi juga akan hancur. Tersangka penghancuran bulan pun memberi syarat kepada pemerintah.</Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Ia akan mengajar di kelas 3E SMP Kunugigaoka. Ia menggunakan nama Koro ketika menjadi guru. Siapapun yang bisa membunuh Koro maka akan mendapatkan hadiah 10 milyar Yen.Meski begitu, sebagian besar murid merasa nyaman ketika diajar oleh Koro Sensei (Guru).</Text>,nomor:9},
                {isi:<Text style={styles.teks}>         Terkuak fakta menarik di sinopsis Ansatsu Kyoushitsu ini. Rupanya kelas 3E SMP Kunugigaoka adalah kumpulan siswa gagal.</Text>,nomor:10},
                {isi:<Text style={styles.teks}>         Bahkan bisa dibilang masa depan anak-anak kelas 3E ini suram. Tidak ada yang mau mendekati dan bergaul dengan mereka</Text>,nomor:11},
                {isi:<Text style={styles.teks}>         Padahal kelas 3E ini memiliki satu kelebihan di bidangnya masing-masing. Ada yang hanya jago baseball, Kimia dan lainnya. Namun pada pelajaran lain sangat buruk.</Text>,nomor:12},
                {isi:<Text style={styles.teks}>         Hingga akhirnya muncul ide gila dari teman Nagisa. Ia harus melakukan bunuh diri agar Koro Sensei ini mati.Nagisa pun rela melakukannya karena apa lagi yang bisa dilakukan kelas gagal tersebut. Namun rencana itu gagal membunuh Koro Sensei.</Text>,nomor:13},
                {isi:<Text style={styles.teks}>         Cara pembunuhan guru mereka di sinopsis Ansatsu Kyoushtsu pun berlanjut. Melihat tak ada perkembangan signifikan, Karasuma pun masuk menjadi guru juga di kelas 3E.Ia mengajari anak-anak itu berbagai teknik membunuh. Tiba-tiba ada yang menyapa Nagisa, Ia bernama Karma. Murid baru yang memang ingin membunuh seorang guru.</Text>,nomor:14},
                {isi:<Text style={styles.teks}>         Namun, lambat laun, Karma pun berubah menjadi siswa yang jauh lebih baik. Hal itu tak lain karena cara pembelajaran dari Koro Sensei.</Text>,nomor:15},
                {isi:<Text style={styles.teks}>         Dalam sinopsis Ansatsu Kyoushitsu muncul sosok pembunuh baru. Ia bernama Irina Jelavich seorang wanita yang cantik dan menguasai 10 bahasa.</Text>,nomor:16},
                {isi:<Text style={styles.teks}>         Ia menyamar sebagai guru untuk mengajar anak-anak kelas 3E. Namun, anak-anak tak begitu suka karena ia hanya fokus membunuh.</Text>,nomor:17},
                {isi:<Text style={styles.teks}>         Saking kesalnya, anak-anak pun memplesetkan namanya Jelavich menjadi Bitch. Hingga akhirnya ia pun mendekati Koro Sensei dan mencoba membunuhnya di gudang dengan senjata asli.</Text>,nomor:18},
                {isi:<Text style={styles.teks}>         Namun, semuanya gagal, Ia malah didandani dengan pakaian sopan oleh Koro Sensei. Hingga akhirnya, Irina pun mengajar murid-murid tersebut dengan baik.</Text>,nomor:19},
                {isi:<Text style={styles.teks}>         Anak-anak kelas 3E mendapatkan pelajaran dari 3 orang hebat. Hal ini jelas membuat mereka menjadi lebih baik.</Text>,nomor:20},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:21},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:22},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/assasion.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default assassion;