import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class opm extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Di planet super benua yang tidak disebutkan namanya, monster aneh dan supervillains secara misterius muncul dan menyebabkan bencana. Untuk memerangi mereka, pahlawan dunia telah bangkit untuk melawan mereka. Saitama adalah salah satu pahlawan yang mampu mengatasi mereka. Ia berasal dari kota metropolitan Z-City dan dengan mudah mengalahkan monster dan penjahat dengan satu pukulan. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Namun, ia bosan dengan kekuatan supernya karena musuhnya yang terlalu lemah baginya dan hanya menjadi sangat bersemangat saat melawan musuh kuat yang dapat menantangnya. Selama serial berlangsung, Saitama bertemu berbagai pahlawan super, supervillains, dan monster. Dia memiliki murid dalam bentuk cyborg bernama Genos dan pada akhirnya mereka bergabung dengan Hero Association untuk mendapatkan pengakuan resmi. </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Selain itu, Saitama juga menghancurkan meteor yang masuk, mengalahkan penjahat bos seperti Deep Sea King. Ketika alien Dark Matter Thieves menyerang dan menghancurkan City A, Saitama mengalahkan pemimpin mereka Lord Boros yang menggunakan pukulan serius.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Saitama mengenal pahlawan super lainnya dalam kelompok tersebut, menjadi teman dengan seniman bela diri dan seorang superhero bernama King yang sebenarnya adalah seorang otaku yang tidak mengenal kekerasan. Ketika eksekutif Hero Association mencoba merekrut penjahat untuk menjadi pahlawan super, seorang penjahat bernama Garo muncul dan mulai memukul banyak pahlawan lainnya, mendorong asosiasi tersebut untuk melakukan sedikit usaha untuk menghentikannya. </Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Untuk mempelajari lebih lanjut tentang seni bela diri, Saitama memasuki sebuah turnamen. Monster yang lebih ganas mulai muncul di berbagai kota, mengenakan banyak pahlawan. Sekelompok dari mereka menculik seorang anak sponsor Asosiasi. Asosiasi Pahlawan belajar bahwa monster telah mengorganisir menjadi Asosiasi Monster, dan mereka juga merekrut anggota dengan memiliki pejuang dan yang lainnya menelan sel-sel rakasa yang mengubahnya menjadi monster yang memiliki kekuatan jauh lebih banyak.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Selama perjalanan Garo dan Asosiasi Pahlawan yang saling terkait, ditemukan bahwa monster telah membuat markas mereka di daerah yang ditinggalkan di Z-City, yang kebetulan hanya didiami oleh Saitama saja. Garo menemukan semua S-Class, atau pahlawan terkuat peringkatnya, dan melanjutkan untuk melanjutkan proses 'monstrifikasi' selama pertempuran. </Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Akhirnya dia bertemu dengan Saitama (yang telah terganggu dengan kebisingan dari pertarungan), yang sikap dan kekuatannya menyebalkan dan akhirnya menghancurkan kehendak Garo, kemudian mengungkapkan visinya untuk masa depan. Saitama, yang keluar dari karakter, mengerti rasa sakitnya, dan di bawah ancaman pahlawan yang kalah untuk membunuh Garo, Garo pergi sebelum ada yang bisa menangkapnya. </Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:8},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:9},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/opm.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default opm;