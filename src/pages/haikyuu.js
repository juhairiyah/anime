import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class haikyu extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Haikyu merupakan sebuah anime sport yang layak ditonton untuk kamu yang suka dengan olahraga terutama bola voli. Anime ini menceritakan seorang siswa SMU KARASUNO Hinata Shouyo, Hinata yang gemar bermain voli karena melihat cara SMU KARASUNO bermain di kejuaraan nasional dan mengidolakan ace mereka yang disebut sebagai "raksasa kecil".</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Pada masa smp Hinata hanya bermain di kejuaraan satu kali dan dikalahkan oleh SMP kitagawa daichi dengan pemain andalan mereka "raja lapangan" Kageyama Tobio. Berniat untuk balas dendam kepada Kageyama, Hinata masuk ke SMU Karasuno. Tanpa disangka-sangka Kageyawa juga memutuskan untuk masuk ke SMU Karasuno. Bumbu - bumbu persaingan diantara mereka sudah terlihat dari awal episode. Seiring berjalannya waktu mereka dikenal dengan duo aneh karena lemparan dan spike mereka yang terlampau cepat.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Hinata yang memiliki bakat dengan kecepatan dan reflek yang luar biasa sementara Kageyama yang memiliki bakat seorang setter jenius dapat membawa Karasuno ke langit nasional setelah beberapa tahun gagal menembus kejuaraan nasional. Kisah kocak kedua orang ini pun di perkuat dengan para kakak kelas yang "terlalu bersemangat".</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:4},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:5},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/haikyuu.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default haikyu;