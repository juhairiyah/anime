import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class ghoul extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Ken Kaneki, yang hampir tidak dapat bertahan hidup setelah pertemuan mematikan dengan Rize Kamishiro, seorang wanita yang diturunkan menjadi ghoul, yaitu makhluk mirip manusia yang memburu dan memakan daging manusia, dibawa ke rumah sakit dalam kondisi kritis. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Setelah sembuh, Kaneki menemukan bahwa entah bagaimana ia menjalani operasi yang mengubahnya menjadi setengah ghoul, dan seperti mereka, harus mengkonsumsi daging manusia untuk bertahan hidup juga. </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Dengan tidak ada orang lain untuk berpaling, ia diambil oleh ghoul yang mengelola kedai kopi "Anteiku", orang pertama yang menemukannya oleh Yoshimura dan Touka Kirishima yang mengajarinya untuk menghadapi kehidupan barunya sebagai setengah manusia/setengah ghoul, termasuk berinteraksi dengan masyarakat ghoul dan yang bertentangan faksi, sementara berjuang untuk menjaga rahasia identitasnya dari manusia lainnya.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Di sana ia betemu dengan Tsukiyama Shuu yang berniat memakannya, dan seniornya di kampus, Nishiki Nishio dan pacarnya Kimi.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Serial prekuel Tokyo Ghoul: JACK menceritakan pemuda bernama Kishou Arima dan Taishi Fura, dua karakter dari serial utama yang saling berkenalan ketika mereka bergabung untuk menyelidiki kematian teman Taishi oleh tangan ghoul, mengarah ke Taishi yang akhirnya mengikuti jalan Arima dan bergabung dengan CCG (Komisi Penanggulangan Ghoul), agen federal yang bertugas dalam menangani kejahatan yang berkaitan dengan ghoul juga.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Serial sekuel Tokyo Ghoul: re menceritakan tentang Kaneki yang amnesia dengan identitas baru Haise Sasaki, pemimpin tim khusus penyidik CCG disebut "Quinx Squad", yang menjalani prosedur yang sama seperti Kaneki, yang memungkinkan mereka untuk mendapatkan kemampuan khusus dari ghoul di CCG untuk melawan mereka, tapi masih bisa hidup sebagai manusia normal karena RC Sell mereka yang kurang lebih masih seperti manusia normal.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:7},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:8},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/tokyoghoul.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default ghoul;