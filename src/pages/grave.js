import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class grave extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         ‘Grave of the Fireflies’ adalah anime produksi Ghibli Studio pada tahun 1988. Grave of the Fireflies berarti kuburan kunang-kunang, film ini merupakan animasi yang sangat sedih. Anime ini mengisahkan tentang kakak beradik yang merupakan korban perang dunia II dimana Amerika menyerang Jepang yang harus bertahan hidup di tengah sulitnya keadaan.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Film ini diawali dari tokoh utamanya yaitu Seita yang terbaring di pinggir stasiun kereta api dengan keadaan yang menyedihkan. Seita akhirnya mati sambil memeluk kaleng bekas permen buah. </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Petugas stasiun yang sedang membersihkan stasiun melihat Seita mati, menemukan kaleng bekas permen buah yang berisi abu ditubuh seita, petugas tersebut mengambilnya lalu membuangnya ke rumput dekat stasiun. </Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Dari kaleng itu muncullah kunang-kunang dan juga seorang gadis kecil yang hendak menghampiri tubuh kakaknya yang terkapar di stasiun, tapi tidak jadi karena kakanya sudah dibelakangnya.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Seita dan Setsuko menumpangi kereta api, Setsuko terlihat senang saat memakan permen buah dari kaleng. Keduanya melihat keluar jendela kereta api, melihat pesawat perang dari Sekutu. Lalu dimulailah cerita dari masa lalu keduanya.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Pada saat itu Seita sedang mengubur bahan makanan sekutu meluncurkan serangan dari udara, ia segera menggendong adik kecilnya Setsuko. Ibunya mengingatkan agar mereka segera ke pengungsian dan meminta agar Setsuko agar jadi anak yang baik dan penurut.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Seita tak sempat membawa banyak barang karena serangan dari udara telah membakar rumah-rumah disekitar tempat tinggalnya. Ia berlari bersama sang adik digendongannya. Kota telah porak poranda akibat serangan, Seita dan Setsuko akhirnya diungsikan ke bangunan sekolah, akibat serangan itu ibu dari keduanya terkena luka bakar yang sangat serius dan seorang petugas menyarankan agar dibawa kerumah sakit. </Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Namun sebelum dibawa ke rumah sakit, ibunya sudah meninggal dunia. Akhirnya Seita pulang kerumah bibinya dan merahasiakan kematian sang ibu dari setsuko. Ia terus mengatakan bahwa ibunya sakit dan akan membawanya menemui ibunya saat keadaan ibunya membaik.</Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Kehidupan Seita dan Setsuko dirumah bibinya berjalan baik, lama-kelamaan saat persediaan makanan semakin berkurang dan jatah beras dari pemerintah hanya sedikit. Sang bibi menjual kimono-kimono ibu Seita untuk ditukarkan dengan beras dan membagi duanya dengan seita. Saat Seita sudah tidak punya apa-apa dan keadaan dirumah bibinya mulai memburuk. Lama –kelamaan bibinya mulai menunjukkan sikap tak sukanya pada seita dan setsuko.Hingga suatu hari Seita memutuskan untuk pergi dari rumah bibinya.</Text>,nomor:9},
                {isi:<Text style={styles.teks}>         Seita menemukan sebuah tempat perlindungan mirip gua di tepi danau dan memutuskan untuk tinggal disitu. Ditempat tinggal mereka banyak kunang-kunang yang dijadikan penerang mereka setiap malam. Persediaan makanan semakin habis dan para petani tidak mau lagi menjual beras. Karena kelaparan, Seita akhirnya mencuri dari ladang petani namun ketahuan oleh pemilik ladang dan membawanya ke kantor polisi.</Text>,nomor:10},
                {isi:<Text style={styles.teks}>         Pada suatu hari Setsuko mengatakan bahwa ia sakit perut dan merasa aneh, Setsuko jadi sakit dan tubuhnya makin kurus dan lemah. Seita membawa adiknya kedokter dan dokter tersebut mengatakan adiknya terkena malnutrition (kekurangan gizi) dan perlu makanan yang bergizi. Saat Seita pulang mencari makanan, ia menemukan adiknya pingsan di tepi danau dan mulai saat itu ia hanya terbaring lemah.</Text>,nomor:11},
                {isi:<Text style={styles.teks}>         Seita pergi ke bank untuk mengambil uang, disitu ia mendengar bahwa Jepang telah menyerah dan kapal pasukan laut dimana ayahnya salah satu tentara tenggelam tanpa sisa, artinya sang ayah meninggal. Ia pulang dengan keadaan terpukul dan menemukan adiknya terbaring sambil mengulum kelerang yang dianggap permen. Setsuko juga mengatakan bahwa ia membuat nasi kepal untuk kakaknya, padahal yang dikasih adalah batu. Seita menyuap semangka ke mulut adiknya dan mengatakan akan memasak bubur dan telur.</Text>,nomor:12},
                {isi:<Text style={styles.teks}>         Diakhir cerita, Seita dan Setsuko duduk di bangku, Seita mengatakan “ seksuko sekarang saatnya tidur” dan ia merebahkan kepalanya dipangkuan Seita dan Setsuko menatap kearah rumahnya yang telah menjadi kota besar dengan bangunan pencakar langit.</Text>,nomor:13},
                {isi:<Text style={styles.teks}>         Film ini mengajarkan banyak hal seperti apa itu melindungi, menyayangi, mendewasakan diri dan cara bertahan hidup. Walaupun menampilkan animasi- animasi yang indah dan penuh warna, namun cerita film ini menampilkan kisah yang sedih. Film ini juga mengajarkan bahwa hidup dengan cara Seita dapat berarti kematian.</Text>,nomor:14},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:15},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:16},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/grave.jpeg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default grave;