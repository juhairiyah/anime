import React, {useEffect} from'react';
import {Image, StyleSheet, Text,View} from 'react-native';


const Splash =({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Login');
        }, 5000);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={require('../asset/anime.jpg')} style = {styles.logo}/>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper:{
        flex : 1,
        backgroundColor: 'white',
        alignItems:'center',
        justifyContent: 'center',
    },
    logo:{
        width:400,
        height:400
    },
    welcomeText:{
        fontSize: 30,
        fontWeight: 'bold',
        color: 'black',
        paddingBottom: 20,
        alignItems:'center'

    },
});