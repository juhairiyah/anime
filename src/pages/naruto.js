import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class jujutsu extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Cerita dimulai ketika seekor monster rubah ekor sembilan atau disebut Kyuubi menyerang Konoha, sebuah desa shinobi yang terletak di negara Api. Kekacauan terjadi di desa Konoha dan korban banyak berjatuhan. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Akhirnya ada seseorang yang berhasil melenyapkan Kyuubi dengan menyegel sebagian chakra Kyuubi itu ke tubuhnya sendiri dan sisanya disegel ke tubuh Naruto, orang yang berhasil menyegel monster rubah ekor sembilan itu dikenal sebagai Yondaime Hokage, Hokage ke-4 atau Minato Namikaze yang tidak lain adalah ayah dari Naruto. Penyegelan itu menggunakan jurus Dewa Kematian sehingga risikonya adalah kematian Hokage ke-4 sendiri.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Dua belas tahun kemudian, diceritakan seorang anak bernama Naruto Uzumaki yang sering membuat onar di desa Konoha. </Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Naruto melakukan hal itu karena menginginkan perhatian dari penduduk desa yang menjauhinya karena rubah di tubuhnya atau disebut sebagai wadah monster berekor atau Jinchuuriki. Naruto tidak mengetahui hal itu, karena Hokage ke-3 melarang penduduk desa Konoha menceritakan serangan Kyuubi tersebut.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Naruto kemudian ditipu oleh Mizuki, seorang pengkhianat Konoha untuk mencuri gulungan rahasia peninggalan Tobirama Senju (Hokage ke-2), Naruto yang polos melakukan hal tersebut dan berhasil mencuri serta mempelajari jurus seribu bayangan. Setelah tahu bahwa dia dimanfaatkan, Naruto menolak memberikan gulungan tersebut.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Lalu, Mizuki menceritakan serangan Kyuubi yang telah menghancurkan Konoha dan menimbulkan banyak korban berjatuhan, termasuk orang tua Iruka. Iruka adalah guru Naruto di akademi yang mana merupakan orang yang pertama kali mengakui keberadaan Naruto, karena dulu Iruka pernah mengalami hal yang sama yaitu hidup tanpa orang tua, dan selalu diselimuti kesendirian. </Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Kemudian Iruka menolong Naruto, namun Iruka terluka parah karena terkena shuriken Mizuki. Naruto yang marah melihat hal itu, sehingga mampu mempraktikkan jurus seribu bayangan yang telah ia pelajari dari gulungan tersebut. </Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Mizuki yang kalah jumlah hingga babak belur karena pukulan bayangan Naruto. Setelah pertarungan tersebut, Iruka memberikan ikat kepalanya kepada Naruto sebagai pengakuan bahwa Naruto pantas menjadi seorang ninja, sehingga Naruto sah menjadi Genin. Sebelumnya, Naruto tidak diizinkan menjadi seorang ninja, bahkan lebih buruknya lagi, Naruto pernah dilarang bersekolah di akademi.</Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:9},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:10},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/naruto.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default jujutsu;