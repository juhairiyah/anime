import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class jujutsu extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Yūji Itadori adalah seorang siswa SMA dengan atletisitas yang tidak wajar yang tinggal di Sendai bersama kakeknya. Ia sering menghindari Klub Lari karena keengganannya pada bidang atletik, meskipun dia memiliki bakat bawaan untuk olahraga tersebut. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Sebaliknya, dia memilih untuk bergabung dengan Klub Penelitian Ilmu Gaib, agar dirinya dapat bersantai dan bergaul dengan para seniornya. Setiap hari, Yūji meninggalkan sekolah pada pukul 17.00 untuk mengunjungi kakeknya di rumah sakit. Ketika dia mengunjunginya, kakeknya memberikan dua pesan kuat kepada Yūji, yaitu "selalu membantu orang" dan "mati dikelilingi orang". </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Setelah kematian kakeknya, Yūji menafsirkan pesan-pesan tersebut sebagai satu pernyataan—bahwa setiap orang berhak mendapatkan "kematian yang layak". Ia kemudian berpapasan dengan Megumi Fushiguro, seorang penyihir (巫, Shaman) yang bertanya kepadanya tentang jimat kutukan tingkat tinggi di sekolahnya yang baru-baru ini ditemukan Yūji.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Senior-seniornya di Klub Penelitian Ilmu Gaib membuka segel dari jimat tersebut dan menemukan jari yang telah membusuk, yang menarik roh Kutukan—makhluk mirip monster yang muncul melalui emosi negatif dan diperkuat dengan mengonsumsi kekuatan sihir yang ada pada penyihir atau jimat semacam itu. Yūji yang tidak mampu mengalahkan roh Kutukan itu karena tidak memiliki kekuatan sihir, menelan jari tersebut untuk melindungi Megumi dan teman-temannya dan menjadi wadah dari Sukuna, roh Kutukan yang kuat.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Karena sifat jahat Sukuna, semua penyihir ditugaskan untuk memusnahkannya dengan segera. Namun, meski dirasuki, Yūji masih mampu mengendalikan tubuhnya. Melihat hal tersebut, Satoru Gojō selaku guru Megumi, memutuskan untuk membawanya ke SMA Jujutsu Metropolitan Tokyo untuk mengusulkan rencana kepada atasannya—menunda hukuman mati Yūji sampai dia mengonsumsi semua jari Sukuna sampai habis, sebelum mereka membunuhnya sekali dan untuk selamanya.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:6},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:7},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/jujutsu.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                       <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default jujutsu;