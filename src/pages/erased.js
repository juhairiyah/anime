import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class erased extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Boku Dake ga Inai Machi (僕だけがいない街, Sebuah Kota Tanpa Diriku), dikenal juga sebagai ERASED, adalah sebuah serial manga seinen Jepang yang ditulis dan diilustrasikan oleh Kei Sanbe dan diserialisasi di majalah Young Ace sejak tahun 2012.[2][3] Volume pertama dirilis pada 26 Januari 2013.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Sebuah adaptasi anime oleh A-1 Pictures mulai tayang di Fuji TV blok Noitamina sejak 8 Januari hingga 25 Maret 2016.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Sebuah serial novel spin-off oleh Hajime Ninomae dirilis di majalah Bungei Kadokawa sejak November 2015 hingga Maret 2016 dan sebuah manga spin-off oleh Sanbe memulai serialisasi di majalah Young Ace pada Juni 2016.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Cerita ini mengikuti Satoru Fujinuma, seorang pemuda yang memiliki kemampuan untuk mengirim dirinya kembali ke waktu sebelum kejadian berbahaya terjadi sehingga memungkinkan dirinya untuk melakukan pencegahan.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Saat ibunya dibunuh, kemampuan Satoru mengirim dirinya ke 18 tahun lalu, saat dia masih di sekolah dasar, memberinya kesempatan untuk mencegah penculikan berantai yang merenggut nyawa kedua teman sekelasnya dan seorang anak kecil dari sekolah lain</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:6},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:7},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/erased.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default erased;