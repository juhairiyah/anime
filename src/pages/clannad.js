import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class clannad extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Clannad menceritakan kisah seorang murid SMA kelas 3 bernama Tomoya Okazaki yang dianggap orang-orang sekitarnya berandalan hanya karena Tomoya sering membolos sekolah. </Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Bagi Tomoya, kehidupannya terasa cukup membosankan.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Sampai akhirnya, pada suatu hari, dia bertemu seseorang yang kurang percaya diri karena dia harus mengulang kelas karena sakit, Nagisa Furukawa. </Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Sejak saat inilah, kehidupan Tomoya mulai mengalami perubahan drastis.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Yuno memperoleh grimoire (buku sihir) empat daun yang sebelumnya dimiliki oleh kaisar sihir pertama dari Kerajaan Semanggi.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Lalu gadis itu mulai mencoba untuk membuat suatu boneka yang terbuat dari sampah dan barang bekas yang dia kumpulkan.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:7},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:8},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/clannad.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                         <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default clannad;