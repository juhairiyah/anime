import React, {Component} from 'react';
import {Image, StyleSheet, Text,View, ScrollView,FlatList, Dimensions,TouchableOpacity} from 'react-native';

const numColumns = 2;
const WIDTH = Dimensions.get('window').width;

class WelcomeAuth extends Component {
    constructor(props){
        super(props);
        this.state = {
            juhai : [
                {anime:<Image source={require('../asset/a-whisker-away_1.jpg')} style={styles.gambar}/>,judul:'A WHISKER AWAY',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('A WHISKER AWAY')}><Text style={{marginTop:10,color:'black',alignSelf:'center'}}>Detail ===</Text></TouchableOpacity>,nomor:1},
                {anime:<Image source={require('../asset/akame.jpg')} style={styles.gambar}/>,judul:'AKAME GA KILL',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('AKAME GA KILL')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:2},
                {anime:<Image source={require('../asset/anohana.jpg')} style={styles.gambar}/>,judul:'ANOHANA',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('ANOHANA')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:3},
                {anime:<Image source={require('../asset/aot.jpg')} style={styles.gambar}/>,judul:'ATTACK ON TITAN',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('ATTACK ON TITAN')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:4},
                {anime:<Image source={require('../asset/arisunosekai.jpg')}  style={styles.gambar}/>,judul:'ANOTHER',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('ANOTHER')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:5},
                {anime:<Image source={require('../asset/assasion.jpg')} style={styles.gambar}/>,judul:'ASSASSINATION CLASSROOM',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('ASSASSINATION CLASSROOM')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:6},
                {anime:<Image source={require('../asset/bc.jpg')} style={styles.gambar}/>,judul:'BLACK CLOVER',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('BLACK CLOVER')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:7},
                {anime:<Image source={require('../asset/clannad.jpg')} style={styles.gambar}/>,judul:'CLANNAD',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('CLANNAD')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:8},
                {anime:<Image source={require('../asset/demon.jpg')} style={styles.gambar}/>,judul:'DEMON SLAYER',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('DEMON SLAYER')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:9},
                {anime:<Image source={require('../asset/erased.jpg')} style={styles.gambar}/>,judul:'ERASED',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('ERASED')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:10},
                {anime:<Image source={require('../asset/grave.jpeg')} style={styles.gambar}/>,judul:'GRAVE OF THE FIREFLIES',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('GRAVE OF THE FIREFLIES')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:11},
                {anime:<Image source={require('../asset/haikyuu.jpg')} style={styles.gambar}/>,judul:'HAIKYUU !!',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('HAIKYUU!!')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:12},
                {anime:<Image source={require('../asset/jujutsu.jpg')} style={styles.gambar}/>,judul:'JUJUTSU KAISEN',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('JUJUTSU KAISEN!!')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:13},
                {anime:<Image source={require('../asset/naruto.jpg')} style={styles.gambar}/>,judul:'NARUTO',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('NARUTO')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:14},
                {anime:<Image source={require('../asset/opm.jpg')} style={styles.gambar}/>,judul:'ONE PUNCH MAN',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('ONE PUNCH MAN')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:15},
                {anime:<Image source={require('../asset/tokyoghoul.jpg')} style={styles.gambar}/>,judul:'TOKYO GHOUL',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('TOKYO GHOUL')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:16},
                {anime:<Image source={require('../asset/tokyorevengers.jpg')} style={styles.gambar}/>,judul:'TOKYO REVENGERS',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('TOKYO REVENGERS')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:17},
                {anime:<Image source={require('../asset/violet.jpg')} style={styles.gambar}/>,judul:'VIOLET EVERGARDEN',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('VIOLET EVERGARDEN')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:18},
                {anime:<Image source={require('../asset/watashini.jpg')} style={styles.gambar}/>,judul:'WATASHI NI TENSHI GA MAIORITA!',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('WATASHI NI TENSHI GA MAIORITA!')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:19},
                {anime:<Image source={require('../asset/yourname.jpg')} style={styles.gambar}/>,judul:'YOUR NAME',detail:<TouchableOpacity style={styles.teks} onPress={() => this.props.navigation.navigate('YOUR NAME')}><Text style={{marginTop:10,color:'black'}}>Detail ===</Text></TouchableOpacity>,nomor:20},
            ],
        }
    }
    render(){
    return(
        <View style={styles.wrapper}>
           <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
               <Text style={{fontSize:30, color:'green'}}>REVIEW ANIME</Text>
           </View>
           <View style={{flex:11}}>
                <FlatList
                    data = {this.state.juhai}
                    renderItem = {({item,index}) => (
                        <View style={styles.ukur}>
                            <Text>{item.anime}</Text>
                            <Text style={{marginTop:10, color:'green'}}>{item.judul}</Text>
                            <Text style={{marginTop:10}}>{item.detail}</Text>
                        </View>
                    )}
                    keyExtractor = {(item) => item.nomor}
                    numColumns = {numColumns}
                />
           </View>
           <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
               <Text style={{fontSize:30, color:'green'}}>ENDUIINGGG</Text>
           </View>
        </View>
    );
    }
};

export default WelcomeAuth;

const styles = StyleSheet.create({
    wrapper:{
        backgroundColor : 'black',
        flex : 1,
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        alignItems:'center',
        padding:5,
        borderRadius:5,
        marginBottom:30,
    },
    gambar:{
        width:200,
        height:200,
    },
    teks : {
        backgroundColor:'green',
        width:200,
        height:40,
        alignItems:'center',
    }
});
