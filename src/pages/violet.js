import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class violet extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Ceritanya berpusat di sekitar para Boneka Kenangan Otomatis (自動手記人形 Jidō Shuki Ningyō): orang-orang yang pada awalnya dipekerjakan oleh seorang ilmuwan bernama Dr. Orland demi membantu istrinya yang buta bernama Mollie untuk menulis novelnya, dan kemudian disewa oleh orang lain yang membutuhkan jasa mereka.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Pada masa sekarang, istilah tersebut merujuk kepada pekerjaan menulis untuk orang lain.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Cerita ini mengisahkan perjalanan Violet Evergarden yang mencoba kembali membaur di tengah kehidupan masyarakat setelah perang usai sekaligus pencarian jati diri dan tujuan hidupnya setelah tidak lagi menjadi prajurit.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:4},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:5},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/violet.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default violet;