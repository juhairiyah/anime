import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class whisker extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Anohana adalah anime tentang persahabatan antara Jintan, Yukiatsu, Anaru, Poppo, Tsuruko, dan Menma yang berubah setelah kematian Menma.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Setelah kematian Menma, mereka berlima berubah sikapnya, bahkan sampai menjauh satu sama lain.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Persahabatan mereka enggak bisa kembali kayak dulu lagi.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Mereka menjalani hidup masing-masing sampai akhirnya bersatu kembali 10 tahun kemudian, saat Menma kembali dalam wujud hantu.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Kisah persahabatan yang enggak lekang dimakan waktu ini, meski udah sempat pisah, saya jamin bakal bikin lo sesenggukan.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Kira-kira, lo bakal percaya enggak kalo ada teman lo yang bilang bahwa teman kalian yang udah lama meninggal balik lagi dalam bentuk hantu dan muncul di hadapannya? Mungkin lo akan menganggap itu cuma bercanda atau bahkan cuma halusinasi dia aja.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Di anime ini, memang cuma Jintan yang bisa lihat Menma, makanya keempat temannya yang lain enggak ada yang percaya awalnya.</Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Saat Jintan enggak dipercaya teman-temannya, emosi Jintan bakal lo rasakan banget.</Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Di anime ini, lo bakal lihat bagaimana persahabatan yang tadinya menyenangkan dan seru bisa berubah saat satu orang aja menghilang. </Text>,nomor:9},
                {isi:<Text style={styles.teks}>         Lo juga bisa lihat bagaimana sebuah keluarga menghadapi kehilangan anggota keluarganya.</Text>,nomor:10},
                {isi:<Text style={styles.teks}>         Cara setiap tokoh menghadapi kehilangan itu berbeda-beda dan alasannya bakal bikin lo meneteskan air mata.</Text>,nomor:11},
                {isi:<Text style={styles.teks}>         Justru karena ceritanya yang sederhana, Anohana jadi bikin perasaan meluap-luap. Kadang, yang lo perlukan untuk bikin film yang sedih bukan tokoh yang disiksa dan diperlakukan semena-mena macam di sinteron yang dramanya berlebihan banget. </Text>,nomor:12},
                {isi:<Text style={styles.teks}>         Di Anohana, cuma dengar para tokohnya bikin pengakuan dengan backsound yang menyentuh aja udah bisa bikin dada lo sesak. </Text>,nomor:13},
                {isi:<Text style={styles.teks}>         Bahkan, bisa dibilang anime ini bergerak lambat banget. Enggak tergesa-gesa, ceritanya mengalir, sampai lo bisa ngerasain perasaan para tokohnya satu per satu.</Text>,nomor:14},
                {isi:<Text style={styles.teks}>         Biarpun saya bilang sederhana, tapi sebenarnya kisahnya cukup kompleks, loh. Bayangin aja, seorang anak perempuan bisa memberikan perasaan begitu besar sama orang-orang terdekatnya sampai mereka sama sekali enggak bisa lepas dari masa lalu.</Text>,nomor:15},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:16},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:17},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/anohana.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default whisker;