import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class whisker extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Tatsumi adalah seorang pejuang yang didampingi oleh dua teman masa kecilnya, Sayo dan Ieyasu, pergi ke ibu kota untuk mencari cara mendapatkan uang demi membantu mengentaskan kemiskinan di desanya.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Setelah terpisah dengan teman-temannya karena serangan bandit, Tatsumi gagal mendaftar menjadi tentara dan harus merasakan kejamnya kehidupan di ibu kota. </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Hidup seperti gelandangan di jalanan, ia lalu diambil oleh keluarga bangsawan, tapi ketika sebuah kelompok pembunuh yang disebut Night Raid menyerang, dia mengetahui bahwa sang tuan rumah ternyata menyiksa dan membunuhnya seperti yang telah mereka lakukan kepada dua temannya, Sayo dan Ieyasu. </Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Akhirnya, ia bergabung dengan Night Raid, yang terdiri dari seorang Ahli Pedang Akame; Petarung Ganas Leone; Gadis Sniper Mine; Pemegang Gunting Sheele; Pemanipulasi Benang Lubbock; Prajurit Baju Besi bernama Bulat; dan pemimpin mereka Najenda, seorang mantan jenderal angkatan darat kekaisaran yang membelot karena tak tahan melihat kekejaman pihak kerajaan pada rakyat.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Night Raid juga adalah bagian dari kekuatan revolusioner yang berkumpul untuk menggulingkan perdana menteri Honest yang secara terang - terangan memanipulasi kaisar muda untuk keuntungan pribadinya meskipun seluruh bangsa jatuh dalam kemiskinan dan perselisihan.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Anggota Night Raid membawa Teigu (帝具, anime: Imperial Arms) (帝具?, anime: Imperial Arms, secara harfiah berarti "Senjata Kerajaan") , yaitu sebuah persenjataan unik yang dibuat 900 tahun lalu dari bahan-bahan yang sangat jarang ditemui, yaitu hewan legendaris yang disebut Danger Beasts (危険種 kikenshu) (危険種 kikenshu?) . </Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Kekuatan Teigu begitu kuat, sehingga ketika dua pengguna Teigu melawan satu sama lain, salah satu dari mereka pasti mati. </Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Meskipun Night Raid berhasil membunuh beberapa Pasukan Khusus Kerajaan, mereka kehilangan Sheele dan Bulat dalam pertarungan melawan anggota model garnisun Seryu dan kelompok yang disebut Tiga Binatang.  </Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Perdana menteri Honest dan kaisar merekrut Esdeath, seorang yang sadis dan pejuang yang kuat dari Utara, untuk memimpin sekelompok prajurit pengguna Teigu yang disebut Jaegers.</Text>,nomor:9},
                {isi:<Text style={styles.teks}>         Night Raid, bersama dengan rekrutan baru, Susanoo dan Chelsea, melawan Jaegers.</Text>,nomor:10},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:11},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:12},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <Image source={require('../asset/akame.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default whisker;