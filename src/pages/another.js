import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class whisker extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Sinopsis anime Another hanya untuk fans anime dewasa. Hal ini karena anime ini benar-benar tidak cocok untuk anak-anak. Bahkan anime ini khusus untuk usia 18 tahun ke atas. Alasannya karena anime Another ini memiliki tingkat kengerian yang luar biasa dan juga sangat horor.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Awal cerita anime Another mulai dari seorang anak yang berada di rumah sakit bernama Sakakibara. Ia merupakan seorang anak yang pindah dari Tokyo ke Yomiyama. Anak ini masih berusia 14 tahun dan akan pindah sekolah ke SMP Yomi Utara.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Perwakilan kelasnya menjenguk Sakakibara. Rupanya Sakakibara belum pernah masuk sekolah. Karena Sakakibara sakit saat akan pergi ke sekolah.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Tige orang teman Sakakibara menjenguknya, yakni ketua kelas bernama Kazami bersama Sakuragi dan Akazawa.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Ruangan tersebut menjadi tegang kala Akazawa bertanya pada Sakakibara. Ia bertanya apakah Sakakibara pernah tinggal di Yomiyama atau tidak. Sakakibara mengatakan pernah ketika kecil.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Seketika ketiga perwakilan kelas merasa lega mendengar hal tersebut. Akhirnya perwakilan kelas itu pun pergi.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Dalam sinopsis Anime Another Sakakibara ingin keluar dari kamar rumah sakit menggunakan lift. Tiba-tiba ia bertemu dengan seorang gadis aneh. Gadis itu menutup matanya sebelah sambil membawa sebuah boneka.</Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Namun Sakakibara tak bertanya apa-apa dan hanya merasa aneh saja. Ia pun kembali ke rumahnya karena sudah sembuh. Sakakibara tinggal bersama dengan kakek dan bibinya yang bernama Mikami Reiko.</Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Kemudian Sakakibara masuk ke kelas 9-3. Kelas ini memiliki aturan mistis. Bibinya pun mengajar di sekolah Yomiyama tersebut. Reiko pun menyebutkan bahwa Sakakibara harus menaati aturan kelas.</Text>,nomor:9},
                {isi:<Text style={styles.teks}>         Selanjutnya dalam sinopsis anime Another, esoknya Sakakibara pun pergi ke sekolah. Ia melihat sebuah bangku kosong di pojokan kelas. Rupanya yang duduk di sana adalah gadis yang matanya tertutup sebelah itu yang bernama Misaki Mei.</Text>,nomor:10},
                {isi:<Text style={styles.teks}>         Saat pelajaran olahraga, Sakakibara bertanya kepada Sakuragi di mana Misaki. Ia pun melihatnya berada di atap sekolah dan menghampirinya.</Text>,nomor:11},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:12},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:13},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/arisunosekai.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default whisker;