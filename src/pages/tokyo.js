import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class tokyo extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Takemichi adalah seorang lelaki pengangguran berusia 26 tahun.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Dia baru mengetahui bahwa gadis yang menjadi pacarnya ketika masih duduk di bangku SMP—satu-satunya gadis yang pernah dikencaninya—telah meninggal akibat ulah geng berandalan. </Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Kemudian, setelah hampir mati tertabrak kereta, dia menyadari bahwa dirinya kembali ke masa lalu saat dia masih duduk di bangku SMP. </Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Takemichi bersumpah untuk mengubah masa depan dan menyelamatkan gadis itu. Demi melakukannya, dia bertujuan untuk naik ke puncak geng berandalan paling brutal di wilayah Kanto.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:5},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:6},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/tokyorevengers.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                       <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default tokyo;