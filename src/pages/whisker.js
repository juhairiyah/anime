import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList, Image} from 'react-native';

class whisker extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         A Whisker Away merupakan sebuah film anime yang disutradarai oleh Jun'ichi Sato dan Tomotaka Shibayama.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Film ini diperankan di antaranya oleh Mirai Shida, Natsuki Hanae dan Susumu Chiba.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         A Whisker Away menceritakan tentang Muge, seorang siswa kelas 2 SMP yang memiliki kepribadian ceria dan penuh energi baik di sekolah maupun di rumahnya.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Muge jatuh cinta pada teman sekelasnya yang bernama Kento Hinode.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Dengan membawa rahasia hatinya itu Muge terus mengejar Kento.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Hingga suatu ketika, Muge menemukan sebuah topeng ajaib yang memungkinkannya untuk berubah menjadi kucing bernama Taro.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Keajaiban itu membuat Muge dapat mendekati Kento, namun akan ada kemungkinan dirinya tak akan pernah menjadi manusia kembali.</Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:8},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:9},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/a-whisker-away_1.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                        <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default whisker;