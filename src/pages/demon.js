import React, {Component} from 'react';
import {View,Text,StyleSheet,TouchableOpacity,FlatList,Image} from 'react-native';

class demon extends Component {
    constructor(props){
        super(props);
        this.state = {
            saya : [
                {isi:<Text style={styles.teks}>         Demon Slayer: Kimetsu no Yaiba merupakan serial anime yang diadaptasi dari seri manga Jepang terlaris berjudul sama. Serial sepanjang 26 episode ini memulai debutnya pada 6 April 2019.</Text>,nomor:1},
                {isi:<Text style={styles.teks}>         Tak kalah dari versi manga-nya, karya sutradara Haruo Sotozaki yang diproduksi Ufotable ini sukses mendapatkan sejumlah penghargaan bergengsi dan dianggap sebagai salah satu serial anime terbaik pada dekade ini.</Text>,nomor:2},
                {isi:<Text style={styles.teks}>         Kisah serial Demon Slayer: Kimetsu no Yaiba bermula saat umat manusia hidup di bawah teror iblis yang bersembunyi dalam kegelapan untuk melahap jiwa-jiwa yang malang. Iblis tersebut adalah manusia yang menjual jiwa mereka dengan imbalan kekuatan.</Text>,nomor:3},
                {isi:<Text style={styles.teks}>         Mereka memiliki kekuatan sihir serta regenerasi dan hidup dengan cara memakan manusia. Karena itu, manusia mulai membentuk kelompok rahasia bernama Demon Slayer Corps yang bertugas memburu dan membunuh iblis.</Text>,nomor:4},
                {isi:<Text style={styles.teks}>         Kelompok tersebut telah melancarkan perang melawan iblis selama berabad-abad. Demon Slayer Corps menggunakan teknik khusus yang memberi mereka kekuatan seperti manusia super.</Text>,nomor:5},
                {isi:<Text style={styles.teks}>         Di sebuah desa, hiduplah seorang bocah bernama Tanjirou Kamado yang merupakan tulang punggung keluarganya. Setibanya di rumah, Tanjirou harus menerima kenyataan pahit lantaran keluarganya telah dibantai oleh iblis.</Text>,nomor:6},
                {isi:<Text style={styles.teks}>         Adiknya, Nezuko, menjadi satu-satunya yang selamat dari serangan mengerikan itu. Namun, Nezuko perlahan berubah menjadi iblis. Pada saat yang sama, ia masih menunjukkan emosi dan pemikiran layaknya manusia.</Text>,nomor:7},
                {isi:<Text style={styles.teks}>         Karena itu, Tanjirou memutuskan untuk bergabung dengan para pembunuh iblis demi menyembuhkan adiknya. Akankah ia berhasil bergabung dengan Demon Slayer Corps dan menyembuhkan Nezuko?</Text>,nomor:8},
                {isi:<Text style={styles.teks}>         Anda bisa menonton anime lengkap tersebut di </Text>,nomor:9},
                {isi:<TouchableOpacity><Text style={{fontSize:20,marginTop:10,color:'blue'}}>https://play.google.com/store/apps/details?id=com.stream.neoanimex </Text></TouchableOpacity>,nomor:10},
            ]
        }
    }
    render(){
        return(
            <View style={{flex:1,backgroundColor:'black'}}>
                <Image source={require('../asset/demon.jpg')} style={{width:200,height:280,marginStart:120}}/>
                <FlatList
                data = {this.state.saya}
                renderItem = {({item,index}) => (
                    <View>
                         <Text style={{color:'green',textAlign:'justify'}}>{item.isi}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    teks : {
        fontSize:20,
        marginTop:10
    }
})
export default demon;